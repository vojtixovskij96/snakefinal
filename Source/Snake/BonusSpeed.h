#pragma once

#include "CoreMinimal.h"
#include "BonusBase.h"
#include "BonusSpeed.generated.h"

/**
 * 
 */

UCLASS()
class SNAKE_API ABonusSpeed : public ABonusBase
{
	GENERATED_BODY()

public:
	virtual void BonusAction(ASnakeBase*Snake) override;
	UPROPERTY(EditDefaultsOnly)
	int32 ScoreFoodBonusSpeed;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float BonusSpeedTimeLife;
	
};
