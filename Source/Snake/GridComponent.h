
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GridComponent.generated.h"

UCLASS()
class SNAKE_API AGridComponent : public AActor
{
	 GENERATED_BODY()
	
public:	
	AGridComponent();
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
	UStaticMeshComponent*Grid;

protected:
	virtual void BeginPlay() override;

public:	

	virtual void Tick(float DeltaTime) override;

};
