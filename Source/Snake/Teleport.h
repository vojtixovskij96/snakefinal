
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Teleport.generated.h"

UCLASS()
class SNAKE_API ATeleport : public AActor,public IInteractable
{
	GENERATED_BODY()
	
public:	
	ATeleport();


protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	virtual void Interact(AActor* Interactor, bool bIsHead)override;

};
