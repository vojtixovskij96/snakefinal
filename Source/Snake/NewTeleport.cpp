// Fill out your copyright notice in the Description page of Project Settings.


#include "NewTeleport.h"
#include"SnakeBase.h"

// Sets default values
ANewTeleport::ANewTeleport()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ANewTeleport::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ANewTeleport::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ANewTeleport::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{

		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->NewTeleport();
		}


	}
}

