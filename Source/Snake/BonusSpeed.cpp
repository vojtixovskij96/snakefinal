#include "BonusSpeed.h"
#include "SnakeBase.h"






void ABonusSpeed::BonusAction(ASnakeBase* Snake)
{
	Snake->MovementSpeed = Snake->MovementSpeed * 0.95f;
	Snake->SetActorTickInterval(Snake->MovementSpeed);
	Snake->SnakeTimeLife = 100.f;
	Snake->Score +=ScoreFoodBonusSpeed;
}
