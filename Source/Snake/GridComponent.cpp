// Fill out your copyright notice in the Description page of Project Settings.


#include "GridComponent.h"

// Sets default values
AGridComponent::AGridComponent()
{
 
	PrimaryActorTick.bCanEverTick = true;
	Grid = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("GridComponent"));
	//RootComponent->SetupAttachment(Grid);
}

// Called when the game starts or when spawned
void AGridComponent::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGridComponent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

