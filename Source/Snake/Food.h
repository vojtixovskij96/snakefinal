#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BonusBase.h"
#include "Components/AudioComponent.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Food.generated.h"
UCLASS()
class SNAKE_API AFood :public ABonusBase
{
	
	GENERATED_BODY()

public:	
	AFood();

	


protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	virtual void BonusAction(ASnakeBase*Snake) override;
	UPROPERTY(EditDefaultsOnly)
	int32 ScoreFoodBonus;
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite)
	float FoodLifeTime;

	
	
};
