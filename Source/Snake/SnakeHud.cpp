// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeHud.h"
#include "Blueprint/UserWidget.h"

ASnakeHud::ASnakeHud(): Super()
{
	ConstructorHelpers::FClassFinder<UUserWidget>WBPSnake(TEXT("/Game/Hud/SnakeBase_WBP"));
	MySnakeHud = WBPSnake.Class;
}

void ASnakeHud::BeginPlay()
{
	Super::BeginPlay();
	UUserWidget* SnakeWidget = CreateWidget(GetWorld(), MySnakeHud);
	SnakeWidget->AddToViewport();
}
