
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GridComponent.h"
#include "Grid.generated.h"


UCLASS()
class SNAKE_API AGrid : public AActor
{
	GENERATED_BODY()
	
public:	
	
	AGrid();
	UPROPERTY(EditDefaultsOnly)
	TArray<AGridComponent*> GridElement;
	UPROPERTY(EditDefaultsOnly)
	 int SizeX;
	 UPROPERTY(EditDefaultsOnly)
	 int SizeY;
	UPROPERTY(EditDefaultsOnly)
	float GridOffset;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AGridComponent> GridElementClass;
protected:
	virtual void BeginPlay() override;

 public:	
	virtual void Tick(float DeltaTime) override;
	
};
