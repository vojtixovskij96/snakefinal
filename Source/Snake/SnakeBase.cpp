
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Teleport.h"
#include "BoxSpawner.h"

ASnakeBase::ASnakeBase()
{
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;
	Score = 0;
	
}

void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	SnakeLifeTime();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	SnakeTimeLife = 100.f;
	for (int i = 0; i < ElementsNum; i++)
	{

		FVector NewLocation(SnakeElements.Num()*ElementSize, 0, 0);
		if (SnakeElements.Num() > 0)
		{
			NewLocation = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation();
		}

		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	FRotator UP(0, 180, 0);
	FRotator Down(0, 0, 0);
	FRotator Left(0, -90, 0);
	FRotator Right(0, 90, 0);
	switch(LastMoveDirection)
	{   
	      case EMovementDirection::UP:
			MovementVector.X += ElementSize;
			SnakeElements[0]->SetActorRotation(UP);
			break;
	      case EMovementDirection::DOWN:
			  SnakeElements[0]->SetActorRotation(Down);
			MovementVector.X -= ElementSize;
			break;
	     case EMovementDirection::LEFT:
			 SnakeElements[0]->SetActorRotation(Left);
			MovementVector.Y += ElementSize;
			break;
	     case EMovementDirection::RIGHT:
			 SnakeElements[0]->SetActorRotation(Right);
			MovementVector.Y -= ElementSize;
			break;
	}
	

	   SnakeElements[0]->ToggleCollision();
	   for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FRotator PrevRotation = PrevElement->GetActorRotation();
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
		CurrentElement->SetActorRotation(PrevRotation);
	}
	     SnakeElements[0]->AddActorWorldOffset(MovementVector);
		 SnakeElements[0]->ToggleCollision();
}
void ASnakeBase::DestroySnake()
{
	for (int i = SnakeElements.Num()-1; i > 0; i--)
	{
		SnakeElements[i]->Destroy();
	}
	this->Destroy();
}

void ASnakeBase::Teleport()

{
	FVector MovementVector(ForceInitToZero);
	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		FVector NewLocation( -9, -1444, 0);
		SnakeElements[0]->SetActorLocation(NewLocation);
	}
	
}

void ASnakeBase::NewTeleport()
{
	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		FVector NewLocationTeleport(-9, 1451, 0);
		SnakeElements[0]->SetActorLocation(NewLocationTeleport);
	}

}


void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement,AActor*Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this,bIsFirst);
		}
	}
	
}

void ASnakeBase::SnakeLifeTime()
{
	SnakeTimeLife = SnakeTimeLife - SnakeSpeedTimeLife;
	if (SnakeTimeLife == 0)
	{
		Destroy();
	}
}





