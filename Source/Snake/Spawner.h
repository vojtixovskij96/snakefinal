

#pragma once
#include "BonusBase.h"
#include "Block.h"
#include "CoreMinimal.h"
#include "GridComponent.h"
#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

class ASnakeBase;
UCLASS()
class SNAKE_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	

	ASpawner();

protected:
	
	virtual void BeginPlay() override;

private:
	TArray<FVector> LocationForBonus;

public:	
	
	virtual void Tick(float DeltaTime) override;
	UPROPERTY(EditDefaultsOnly)
	int32 SizeX;
	UPROPERTY(EditDefaultsOnly)
	int32 SizeY;
	UPROPERTY(EditDefaultsOnly)
	float Padding;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<TSubclassOf<ABonusBase>> BonusTypeArray;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AGridComponent> Grid;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ABlock> BlockElement;
	
	
	UFUNCTION()
	void SpawnBonus();
	void GridSpawn();
	void SpawnBlock();
	


};
