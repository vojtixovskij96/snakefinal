
#pragma once

#include "CoreMinimal.h"
#include "GridComponent.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT,
};


UCLASS()
class SNAKE_API ASnakeBase : public AActor
{
	GENERATED_BODY()

public:
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;
	
	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
	EMovementDirection LastMoveDirection;
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	float SnakeTimeLife;
	UPROPERTY(EditDefaultsOnly)
	float SnakeSpeedTimeLife;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	int32 Score;
	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override
	{
		Super::BeginPlay();
		SetActorTickInterval(MovementSpeed);
		AddSnakeElement(3);
		
	}

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElement(int ElementsNum = 1);
	void Move();
	void DestroySnake();
	void Teleport();
	void NewTeleport();
	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
	void SnakeLifeTime();



	
};
