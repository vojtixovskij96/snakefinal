
#include "BoxSpawner.h"
#include "Kismet/KismetMathLibrary.h"


ABoxSpawner::ABoxSpawner()
{
	SpawnBox = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxSpawner"));
	SpawnBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}
void ABoxSpawner::BeginPlay()
{
	Super::BeginPlay();
	
}

void ABoxSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABoxSpawner::SpawnFood()
{
	FRotator Rotation(0,0,0);
	FVector RandomLocation = UKismetMathLibrary::RandomPointInBoundingBox(GetActorLocation(), SpawnBox->GetScaledBoxExtent());
	GetWorld()->SpawnActor<AActor>(SpawnClass,RandomLocation, Rotation);
}

