// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SnakeHud.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_API ASnakeHud : public AHUD
{
	GENERATED_BODY()
public:
	ASnakeHud();

	virtual void BeginPlay() override;
	TSubclassOf<class UUserWidget> MySnakeHud;
};
