
#include "Teleport.h"
#include"SnakeBase.h"


ATeleport::ATeleport()
{
 

}

// Called when the game starts or when spawned
void ATeleport::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATeleport::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATeleport::Interact(AActor* Interactor, bool bIsHead)
{

	if (bIsHead)
	{

		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{ 
			Snake->Teleport();
		}

		
	}
}

