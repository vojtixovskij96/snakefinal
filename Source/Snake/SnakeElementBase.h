
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeElementBase.generated.h"

class USaticMeshComponent;
class ASnakeBase;
UCLASS()
class SNAKE_API ASnakeElementBase : public AActor, public IInteractable
{

	GENERATED_BODY()
	
public:	
	ASnakeElementBase();

	UPROPERTY(VisibleAnyWhere,BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;
	UPROPERTY()
	ASnakeBase* SnakeOwner;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeDeadElements;
	UPROPERTY(EditDefaultsOnly)
	TArray<ASnakeElementBase*> SnakeDeaElements;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

UFUNCTION(BlueprintNativeEvent)
void SetFirstElementType();
void SetFirstElementType_Implementation();
virtual void Interact(AActor* Interactor,bool bIsHead) override;
UFUNCTION()
void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult);
UFUNCTION()
void ToggleCollision();
UFUNCTION()
void ToggleVisible();

};
