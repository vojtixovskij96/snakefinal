// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UserWidgetSnake.generated.h"

class ASnakeBase;
UCLASS()
class SNAKE_API UUserWidgetSnake : public UUserWidget
{
	GENERATED_BODY()
public:
	

	TSubclassOf<ASnakeBase> SnakeGame;
};
