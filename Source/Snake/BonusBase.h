// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Components/AudioComponent.h"
#include "SnakeBase.h"
#include "BonusBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FMyDelegate);

UCLASS()
class SNAKE_API ABonusBase : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABonusBase();
	FMyDelegate BonusActivated;
	UPROPERTY(EditDefaultsOnly)
	UAudioComponent* MyAudioComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Destroyed() override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION()
	virtual void BonusAction(ASnakeBase* Snake);
	


};
