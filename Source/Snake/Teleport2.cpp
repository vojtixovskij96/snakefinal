// Fill out your copyright notice in the Description page of Project Settings.


#include "Teleport2.h"
#include"SnakeBase.h"

// Sets default values
ATeleport2::ATeleport2()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	

}

// Called when the game starts or when spawned
void ATeleport2::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATeleport2::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATeleport2::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{

		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->NewTeleport();
		}


	}
}

