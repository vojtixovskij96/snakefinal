
#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "BoxSpawner.generated.h"

UCLASS()
class SNAKE_API ABoxSpawner : public AActor
{
	GENERATED_BODY()
public:
	UBoxComponent*SpawnBox;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AActor>SpawnClass;
	UPROPERTY(EditDefaultsOnly)
	float SpawnObjectCout;
	UPROPERTY(EditDefaultsOnly)
	bool bShouldSpawnLoop;

public:	
	ABoxSpawner();
	

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	void SpawnFood();



};
