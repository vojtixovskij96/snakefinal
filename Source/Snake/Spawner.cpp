


#include "Spawner.h"


ASpawner::ASpawner()
{
	PrimaryActorTick.bCanEverTick = true;
	


}


void ASpawner::BeginPlay()
{
	GridSpawn();
	SpawnBonus();
	SpawnBlock();
	
}


void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpawner::SpawnBonus()
{
	
	if (BonusTypeArray.Num())
	{
		
		int32 RandomBonusIndex = FMath::RandRange(0, BonusTypeArray.Num() - 1);
		auto ClassForSpawn = BonusTypeArray[RandomBonusIndex];
		int32 RandomLocationIndex = FMath::RandRange(0, LocationForBonus.Num() - 1);
		auto SpawnLocation = LocationForBonus[RandomLocationIndex];
		auto CurrentBonus = GetWorld()->SpawnActor<ABonusBase>(ClassForSpawn, FTransform(SpawnLocation));
		CurrentBonus->BonusActivated.AddDynamic(this, &ASpawner::SpawnBonus);
		

	}

}

void ASpawner::GridSpawn()
{
	const FVector Location = GetActorLocation();
	const FVector CenterOffset = FVector((SizeX - 1) * Padding * 0.5f, (SizeY - 1) * Padding * 0.5f, 0.f);


	for (int32 i = 0; i < SizeX; i++)
	{
		for (int32 j = 0; j < SizeY; j++)
		{

			FVector SpawnLocation = Location + FVector(Padding * i, Padding * j, 0) - CenterOffset;
			GetWorld()->SpawnActor<AGridComponent>(Grid, FTransform(SpawnLocation));
			LocationForBonus.Add(SpawnLocation);
		}
	}
}

void ASpawner::SpawnBlock()
{
	for (int32 i = 0; i < (SizeY /2+(SizeX/4)); i++)
	{
		int32 RandomLocationIndex = FMath::RandRange(0, LocationForBonus.Num() - 1);
		auto SpawnLocation = LocationForBonus[RandomLocationIndex];
		LocationForBonus.RemoveAt(RandomLocationIndex);
		GetWorld()->SpawnActor<ABlock>(BlockElement, FTransform(SpawnLocation));
	}
}



