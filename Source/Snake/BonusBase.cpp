// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusBase.h"

// Sets default values
ABonusBase::ABonusBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MyAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));
	MyAudioComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	MyAudioComponent->bAutoActivate = true;
}

// Called when the game starts or when spawned
void ABonusBase::BeginPlay()
{
	Super::BeginPlay();
	
}

void ABonusBase::Destroyed()
{
	BonusActivated.Broadcast();
	Super::Destroyed();
}

void ABonusBase::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		BonusAction(Snake);
		MyAudioComponent->Play();
		Destroy();
	}

}

// Called every frame
void ABonusBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABonusBase::BonusAction(ASnakeBase* Snake)
{
	Destroy();

}

